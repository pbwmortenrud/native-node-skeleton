'use strict';

export const menu = [
    {
        text: "Home",
        url: "/"
    },
    {
        text: "Page",
        url: "/page1"
    },
    {
        text: "Contacts",
        url: "/contacts"
    },
,
    {
        text: "New Contact",
        url: "/contact"
    },
    {
        text: "About",
        url: "/about"
    }
];